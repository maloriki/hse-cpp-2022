# Список экзаменационных билетов

## Первый семестр
+ Declarations, definitions and control sequences in C++
+ Expressions and operators in C++
+ Arrays, pointers and references in C++
+ Structure and class definitions: member fields, methods, constructors and destructors
+ Encapsulation, private and public keywords, class operators overloading
+ Templates
